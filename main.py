import sys
from PyQt4 import QtGui
from datetime import timedelta, date
import PyQtHelper
import TacoManager


def strToTimedelta(time, seperator=':'):
    values = [int(value) for value in time.split(seperator)
              if value]

    for value in values:
        print value

    if len(values) == 3:
        return timedelta(hours=values[0],
                         minutes=values[1],
                         seconds=values[2])
    elif len(values) == 2:
        return timedelta(minutes=values[0],
                         seconds=values[1])
    elif len(values) == 1 and not values[0]:
        return timedelta(seconds=values[0])
    else:
        return None


class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.initUI()

    def initUI(self):
        self.resize(400, 500)
        self.center()
        self.setWindowTitle("Taco Manager")

        self.grid = QtGui.QGridLayout()
        self.grid.setSpacing(10)

        self.statusBar()
        menubar = self.menuBar()
        menubar.addMenu(self._createFileMenu())

        self._createDriverMenu()
        self._createLorriesMenu()
        self._createTimeForm()
        self._createDistanceForm()
        self._createCommentForm()
        self._createControls()

        widget = QtGui.QWidget(self)
        widget.setLayout(self.grid)
        self.setCentralWidget(widget)

    def center(self):
        frameGeom = self.frameGeometry()
        center = QtGui.QDesktopWidget().availableGeometry().center()
        frameGeom.moveCenter(center)
        self.move(frameGeom.topLeft())

    def createDriver(self):
        name, ok = QtGui.QInputDialog.getText(self,
                                              "New Driver",
                                              "Driver name:")

        if ok:
            if name.count(' '):
                firstName, lastName = name.split(" ")
            else:
                firstName = name
                lastName = ""

            TacoManager.createDriver(firstName, lastName)
            self._updateDriverMenu()

    def createLorry(self):
        licensePlate, ok = QtGui.QInputDialog.getText(self,
                                                      "New Lorry",
                                                      "License Plate")

        if ok:
            TacoManager.createLorry(licensePlate)
            self._updateLorriesMenu()

    def addActivity(self):
        driver = self.driversMenu.currentIndex()
        lorry = self.lorriesMenu.currentIndex()
        startTime = strToTimedelta(self.startTime.text())
        endTime = strToTimedelta(self.endTime.text())
        drivingTime = strToTimedelta(self.drivingTime.text())
        otherTime = strToTimedelta(self.otherTime.text())
        startKilo = self.startKilometers.text()
        endKilo = self.endKilometers.text()
        comment = self.comment.text()

        if not startTime:
            self._showErrorMessage("Start time is not valid")
            return

        if not endTime:
            self._showErrorMessage("End time is not valid")
            return

        if not drivingTime:
            self._showErrorMessage("Driving time is not valid")
            return

        if not otherTime:
            self._showErrorMessage("Other time is not valid")
            return

        if not startKilo:
            self._showErrorMessage("Starting Kilometers is not valid")
            return

        if not endKilo:
            self._showErrorMessage("Ending Kilometers is not valid")
            return

    def _nextRow(self):
        if not hasattr(self, "row"):
            self.row = 1
        else:
            self.row += 1

        return self.row

    def _createDriverMenu(self):
        self.driversMenu = QtGui.QComboBox()
        driversLabel = QtGui.QLabel("Driver:")

        row = self._nextRow()

        self.grid.addWidget(driversLabel, row, 0)
        self.grid.addWidget(self.driversMenu, row, 1, 1, 3)

        self._updateDriverMenu()

    def _updateDriverMenu(self):
        PyQtHelper.removeAllItems(self.driversMenu)

        drivers = TacoManager.getDrivers()

        for (counter, driver) in enumerate(drivers):
            self.driversMenu.addItem(driver, counter)

    def _createLorriesMenu(self):
        self.lorriesMenu = QtGui.QComboBox(self)
        lorryLabel = QtGui.QLabel("Lorry:")

        row = self._nextRow()

        self.grid.addWidget(lorryLabel, row, 0)
        self.grid.addWidget(self.lorriesMenu, row, 1, 1, 3)

        self._updateLorriesMenu()

    def _updateLorriesMenu(self):
        PyQtHelper.removeAllItems(self.lorriesMenu)
        lorries = TacoManager.getLorries()

        for (counter, lorry) in enumerate(lorries):
            self.lorriesMenu.addItem(lorry, counter)

    def _createTimeForm(self):
        self.date = QtGui.QLineEdit(date.today().strftime("%d/%m/%Y"))
        self.startTime = QtGui.QLineEdit()
        self.endTime = QtGui.QLineEdit()
        self.drivingTime = QtGui.QLineEdit()
        self.otherTime = QtGui.QLineEdit()

        row = self._nextRow()
        self.grid.addWidget(QtGui.QLabel("Date:"), row, 0)
        self.grid.addWidget(self.date, row, 1, 1, 3)

        row = self._nextRow()
        self.grid.addWidget(QtGui.QLabel("Start Time:"), row, 0)
        self.grid.addWidget(self.startTime, row, 1, 1, 3)

        row = self._nextRow()
        self.grid.addWidget(QtGui.QLabel("End Time:"), row, 0)
        self.grid.addWidget(self.endTime, row, 1, 1, 3)

        row = self._nextRow()
        self.grid.addWidget(QtGui.QLabel("Driving Time:"), row, 0)
        self.grid.addWidget(self.drivingTime, row, 1, 1, 3)

        row = self._nextRow()
        self.grid.addWidget(QtGui.QLabel("Other Time:"), row, 0)
        self.grid.addWidget(self.otherTime, row, 1, 1, 3)

    def _createDistanceForm(self):
        self.startKilometers = QtGui.QLineEdit()
        self.finishKilometers = QtGui.QLineEdit()

        row = self._nextRow()
        self.grid.addWidget(QtGui.QLabel("Starting Kilometers:"), row, 0)
        self.grid.addWidget(self.startKilometers, row, 1, 1, 3)

        row = self._nextRow()
        self.grid.addWidget(QtGui.QLabel("Finishing Kilometers:"), row, 0)
        self.grid.addWidget(self.finishKilometers, row, 1, 1, 3)

    def _createCommentForm(self):
        self.comment = QtGui.QTextEdit()

        row = self._nextRow()
        self.grid.addWidget(QtGui.QLabel("Comments:"), row, 0)
        self.grid.addWidget(self.comment, row, 1, 3, 3)
        self.row += 2

    def _createControls(self):
        nextBtn = QtGui.QPushButton("Next")
        nextBtn.clicked.connect(self.addActivity)

        row = self._nextRow()
        self.grid.addWidget(nextBtn, row, 0, 1, 4)

    def _createFileMenu(self):
        fileMenu = QtGui.QMenu('File', self)
        fileMenu.addMenu(self._createNewMenu())
        fileMenu.addAction(self._createExitAction())

        return fileMenu

    def _createNewMenu(self):
        newMenu = QtGui.QMenu('New', self)
        newMenu.addAction(self._createDriverAction())
        newMenu.addAction(self._createLorryAction())

        return newMenu

    def _createDriverAction(self):
        driverAction = QtGui.QAction(QtGui.QIcon.fromTheme("insert-object"),
                                     "Driver", self)
        driverAction.setStatusTip("Add a driver")
        driverAction.triggered.connect(self.createDriver)

        return driverAction

    def _createLorryAction(self):
        lorryAction = QtGui.QAction(QtGui.QIcon.fromTheme("insert-object"),
                                    "Lorry", self)
        lorryAction.setStatusTip("Add a lorry")
        lorryAction.triggered.connect(self.createLorry)

        return lorryAction

    def _createExitAction(self):
        exitAction = QtGui.QAction(QtGui.QIcon("exit.png"), '&Exit', self)
        exitAction.setStatusTip("Exit Application")
        exitAction.triggered.connect(QtGui.qApp.quit)

        return exitAction

    def _showErrorMessage(self, msg):
        QtGui.QMessageBox.critical(self, "Error", msg, "Ok")

def main():
    TacoManager.initializeTables()

    app = QtGui.QApplication(sys.argv)
    window = MainWindow()
    window.show()

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
