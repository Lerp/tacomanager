import database
import datetime

db = database.Database("TacoManager")
drivers = database.Table("DRIVERS")
lorries = database.Table("LORRIES")
activity = database.Table("ACTIVITY")


def initializeTables():
    drivers.addColumn("id", "INTEGER PRIMARY KEY")
    drivers.addColumn("first_name", "TEXT")
    drivers.addColumn("last_name", "TEXT")

    lorries.addColumn("id", "INTEGER PRIMARY KEY")
    lorries.addColumn("license_plate", "TEXT")

    activity.addColumn("driver_id", "INTEGER")
    activity.addColumn("lorry_id", "INTEGER")
    activity.addColumn("start_time", "INTEGER")
    activity.addColumn("end_time", "INTEGER")
    activity.addColumn("driving_time", "INTEGER")
    activity.addColumn("other_time", "INTEGER")
    activity.addColumn("start_kilometers", "INTEGER")
    activity.addColumn("end_kilometers", "INTEGER")
    activity.addColumn("comment", "TEXT")

    db.addTable(drivers)
    db.addTable(lorries)
    db.addTable(activity)


def unixTime(time):
    if type(time) is datetime.timedelta:
        return time.total_seconds()
    elif type(time) is datetime.datetime:
        return (time - datetime.datetime(1970, 1, 1)).total_seconds()
    else:
        raise Exception("Cannot convert type %s to unix time"
                        % (str(time)))


def createDriver(firstName, lastName):
    drivers.insertRow({"first_name": firstName,
                       "last_name": lastName})


def createLorry(licensePlate):
    lorries.insertRow({"license_plate": licensePlate})


def getDriverId(firstName, lastName):
    drivers.find("id", "first_name='%s' AND last_name='%s'"
                 % (firstName, lastName))


def getLorryId(licensePlate):
    lorries.find("id", "license_plate='%s'" % (licensePlate, ))


def addActivity(driver, lorry, startDateTime, endDateTime, drivingTime,
                otherTime, startKilometers, endKilometers, comment):
    startUnix = unixTime(startDateTime)
    endUnix = unixTime(endDateTime)
    drivingUnix = unixTime(drivingTime)
    otherUnix = unixTime(otherTime)

    activity.insertRow({"driver_id": driver,
                        "lorry_id": lorry,
                        "start_time": startUnix,
                        "end_time": endUnix,
                        "driving_time": drivingUnix,
                        "other_time": otherUnix,
                        "start_kilometers": startKilometers,
                        "end_kilometers": endKilometers,
                        "comment": comment})


def getDrivers():
    rows = []

    for row in db.getRows(drivers):
        rows.append("%s %s" % (row[1], row[2]))

    return rows


def getLorries():
    rows = []

    for row in db.getRows(lorries):
        rows.append(row[1])

    return rows

if __name__ == "__main__":
    initializeTables()
    drivers.find("Adrian", "Bates")
