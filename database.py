import sqlite3
import collections
import logger


class Table:
    def __init__(self, name):
        self.name = name
        self.columns = collections.OrderedDict()
        self.database = None

    def setDatabase(self, db):
        self.database = db

    def addColumn(self, name, type):
        self.columns[name] = type

    def insertRow(self, values):
        cols = ", ".join(["'%s'" % (key, ) for key in iter(values)])
        fields = ", ".join(["'%s'" % (values[key], ) for key in iter(values)])

        if self._hasDatabase():
            self.database._execute("INSERT INTO %s (%s) VALUES(%s)"
                                   % (self.name, cols, fields))
        else:
            logger.error("Cannot add row to %s as it does not belong to a"
                         " database" % (self.name, ))

    def find(self, column, clause):
        if not self._hasDatabase():
            logger.error("Cannot perform search on %s because it doesn't"
                         " belong to a database" % (self.name, ))
            return

        self.database._execute("SELECT %s FROM %s WHERE %s"
                               % (column, self.name, clause))

        return self.database.cursor.fetchall()

    def _create(self):
        columns = ", ".join(
            [" ".join([key, value]) for key, value in self.columns.items()]
        )

        self.database._execute("CREATE TABLE %s (%s)"
                               % (self.name, columns))

    def _hasDatabase(self):
        return hasattr(self, "database")


class Database:
    def __init__(self, databaseName):
        self.name = databaseName
        self.connection = sqlite3.connect(databaseName)
        self.connection.isolation_level = None
        self.cursor = self.connection.cursor()

    def addTable(self, table):
        with self.connection:
            self._execute("PRAGMA table_info(%s)" % (table.name))

            if self._nextResult():
                logger.warning("Table \"%s\" already exists" % (table.name, ))
                table.setDatabase(self)
            else:
                logger.message("Table \"%s\" not found." % (table.name, ))
                logger.message("Creating table \"%s\"" % (table.name, ))
                table.setDatabase(self)
                table._create()

    def getRows(self, table):
        self._execute("SELECT * FROM %s" % (table.name, ))

        return self.cursor.fetchall()

    def _execute(self, command):
        self.cursor.execute(command)

    def _nextResult(self):
        return self.cursor.fetchone()
